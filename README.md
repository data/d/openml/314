# OpenML dataset: thyroid_sick

https://www.openml.org/d/314

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
  
**Source**: Unknown - 1987  
**Please cite**:   

Thyroid disease records supplied by the Garavan Institute and J. Ross Quinlan, New South Wales Institute, Sydney, Australia. 1987)

Classes: sick, negative
Num Instances: 3772
Num Attributes: 30
Num Continuous: 7 (Int 1 / Real 6)
Num Discrete: 23
Missing values: 6064 /  5.4%

UCI: http://archive.ics.uci.edu/ml/datasets/Thyroid+Disease

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/314) of an [OpenML dataset](https://www.openml.org/d/314). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/314/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/314/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/314/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

